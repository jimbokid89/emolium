'use strict';
if (!window.console) window.console = {};
if (!window.console.memory) window.console.memory = function() {};
if (!window.console.debug) window.console.debug = function() {};
if (!window.console.error) window.console.error = function() {};
if (!window.console.info) window.console.info = function() {};
if (!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if (!Modernizr.flexbox) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      noFlexboxStickyFooter = function() {
        $pageBody.height('auto');
        if ($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
        } else {
          $pageWrapper.height('auto');
        }
      };
    $(window).on('load resize', noFlexboxStickyFooter);
  })();
}
if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      ieFlexboxFix = function() {
        if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageWrapper.height($(window).height());
          $pageBody.removeClass('flex-none');
        } else {
          $pageWrapper.height('auto');
        }
      };
    ieFlexboxFix();
    $(window).on('load resize', ieFlexboxFix);
  })();
}

$(function() {

  // placeholder
  //-----------------------------------------------------------------------------
  $('input[placeholder], textarea[placeholder]').placeholder();


  $('.js-scroll').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $($.attr(this, 'href')).offset().top - 50
    }, 1500);
  })


  var overlay = $('.overlay');
  var duration = 400;
  var popUp = $('.pop-up');

  $('.js-show-pop-up').on('click', function(e) {

    e.preventDefault();
    $(this).addClass('active');
    var myth = $(this).attr('data-myth');
    overlay.fadeIn(duration);
    setTimeout(function() {
      popUp.fadeIn(duration).css({
        'top': $(document).scrollTop() - 100
      }).addClass('slideInUp animated');

    }, 500);
    popUp.find('.' + myth + '').show();
    $('.red-line-btns').find('.' + myth + '').addClass('visited').find('.rnd-btn').addClass('visited');


        $(this).parent().find('.show-true').addClass('active');




  });


  overlay.on('click', function() {
    closePop();
  });
  $('.js-close-pop').on('click', function(e) {
    e.preventDefault();
    closePop();
  })



  function closePop() {
    overlay.fadeOut(duration);
    popUp.fadeOut(duration);
    setTimeout(function() {
      $('.pop-up-body').hide();
    }, duration)

  }


  //Validation

  $('.email-form').validate({
    rules: {
      email: {
        required: true,
        email: true
      },
      checkbox: {
        required: true
      }
    },
    messages: {
      email: {
        required: 'Заполните поле Email',
        email: 'Введите пожалуйста валидный Email',
      },
      checkbox: {
        required: 'Что бы продолжить вы должны согласиться'
      }
    },
    submitHandler: function() {
      $('.email-pop').fadeIn(800).css({
        'top': $(document).scrollTop() + 100
      });
      setTimeout(function() {
        $('.email-pop').fadeOut(800);
      }, 2500);
    }
  });

  $('.myth-section').on('inview', function(event, isInView) {
    if (isInView) {
      $(this).addClass('animate-start').find('.title-myth').addClass('slideInUp animated').css({
        'opacity': 1
      });
    }
  });

  $(window).on('scroll', function() {
    checkHeight();
  });

  $(window).on('load', function() {
    $('.test-line').addClass('go');
  })



  $('.scroll-down').on('click', function(e) {
    e.preventDefault();
    if ($(this).hasClass('back')) {
      return;
    }


    $('html, body').animate({
      scrollTop: $(window).scrollTop() + $(window).outerHeight()
    }, 1800, function() {
      checkHeight();
    });
  })

  function checkHeight() {
    if ($(window).scrollTop() > ($(document).outerHeight() - 1000)) {
      $('.scroll-down').addClass('back');
    } else {
      $('.scroll-down').removeClass('back');
    }
  }

  $(document).on('click', '.back', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 800);
  });


});
